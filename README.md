
# Yuwen's Personal Zola Page 
This repo is the personal page for IDS721 mini project 1: building a static website using Zola theme.

The theme I am using here is [Adidoks](https://github.com/aaranxu/adidoks).

## Demo
[Live Demo Link](https://yuwen-ids721-zolapage-candice1121-a45a7aea243f53536e4c19dca3c98.gitlab.io/)

## Customization
Change the deafult theme from purplrish to matcha green style

## Deployment:
#### Step 1: Customize Zola theme
Use any zola theme from the official website and fork/template your own customized theme

#### Step 2: Create a new Zola site
Create a repo on Gitlab and follow the instruction on how to  [Deploy Zola on Gitlab](https://www.getzola.org/documentation/deployment/gitlab-pages/)

#### Step 3: Configuration
Copy configuration file and content directory into your own repo and modify them to your own settings.

#### Step 4: Deploy on Gitlab
Create .gitlab-ci.yml and commit and push all content to GitLab and run the link. By default, GitLab Pages are hosted at 
https://username.gitlab.io/project
where <username> is your GitLab username, and <project> is the name of your GitLab project.


## Color Reference

| Color             | Hex                                                                |
| ----------------- | ------------------------------------------------------------------ |
| Matcha Green | ![#8ba888](https://via.placeholder.com/10/8ba888?text=+) #8ba888 |


## Page Screenshot
![Zola Page](page.png)
