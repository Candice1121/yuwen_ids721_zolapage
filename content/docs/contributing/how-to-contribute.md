+++
title = "How to Contribute"
description = "Contribute to AdiDoks, improve documentation, or submit to showcase."
date = 2021-05-01T18:10:00+00:00
updated = 2021-05-01T18:10:00+00:00
draft = false
weight = 410
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Contribute to AdiDoks, improve documentation, or submit to showcase."
toc = true
top = false
+++

👉 Make sure to read the [Code of Conduct](../code-of-conduct/).


## Improve documentation

👉 The documentation lives in [`./content/docs/`](https://github.com/aaranxu/adidoks/tree/master/content/docs)
of the [`adidoks` repository](https://github.com/h-enk/getdoks.org).

