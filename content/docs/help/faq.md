+++
title = "FAQ"
description = "Answers to frequently asked questions."
date = 2021-05-01T19:30:00+00:00
updated = 2021-05-01T19:30:00+00:00
draft = false
weight = 30
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Answers to frequently asked questions."
toc = true
top = false
+++

## What is the this page?

This is the page for IDS721 Mini-project 1 building a static zola page.


## Contact the creator?

Send *Yuwen Cai* an E-mail:

- <yuwen.cai@duke.edu>
